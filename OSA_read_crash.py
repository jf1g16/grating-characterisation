## Test program for running the OSA (longwave) ##
# Sam Berry, 30 Mar 2015

# Store some constants
#HOST = "6370d91r223997.clients.soton.ac.uk"
HOST = "osavis.clients.soton.ac.uk"
PORT = 10001
READBUFF = 1024

# Import relevant modules
import numpy as np
import socket as soc
import time as t
import matplotlib.pyplot as plt
from struct import unpack
import Tkinter  as tk
import tkFileDialog
import os
import winsound

def ensure_dir(file_path):
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)

print 'Initialising'
# Create a socket connection to the host
s = soc.socket(soc.AF_INET, soc.SOCK_STREAM)
s.connect((HOST,PORT))

# Send the beginning of the authentication command
s.send('open "smithgrp"\r\n')

# Receive the response and print to screen (for verification).
data = s.recv(1024)
print data

# Send the password to the OSA and verify it is ready for commands
s.send('QuantumOSA1\r\n')

data2 = s.recv(1024)
print str(data2)

# Send the command for binary (64-bit) data
s.send(':format:data REAL,64\r\n')

def readtrace(trace):
    # Request the Y values of trace A
    s.send(':trac:Y? tr'+trace+'\r\n')
    
    # Initialise a data buffer and retrieve the first packet of data to 'temp'.
    data = ''
    temp = s.recv(READBUFF)
    
    # While the data equals the read limit, continue as there may be more data
    while len(temp) == READBUFF:
        data += temp
        t.sleep(0.2) # Don't like this manual delay TODO: keep reading until data all received
        temp = s.recv(READBUFF)
    
    # Append the final temporary buffer to the data buffer
    data += temp
    
    # Show the length of the buffer
    print len(data)
    
    # Use the binary data format header to separate the data part
    header_size = int(data[1])
    array_size = int(data[2:header_size+2])/8
    
    # Convert the byte data to IEEE 64-bit floating point numbers
    Y = unpack('d'*array_size, data[header_size+2:-2])
    
    # Request the X values of trace A
    s.send(':trac:X? tr'+trace+'\r\n')
    
    # Initialise a data buffer and retrieve the first packet of data to 'temp'.
    data = ''
    temp = s.recv(READBUFF)
    
    # While the data equals the read limit, continue as there may be more data
    while len(temp) == READBUFF:
        data += temp
        t.sleep(0.2) # Don't like this manual delay TODO: keep reading until data all received
        temp = s.recv(READBUFF)
    
    # Append the final temporary buffer to the data buffer
    data += temp
    
    # Show the length of the buffer
    print len(data)
    
    # Use the binary data format header to separate the data part
    header_size = int(data[1])
    array_size = int(data[2:header_size+2])/8
    
    # Convert the byte data to IEEE 64-bit floating point numbers
    X = unpack('d'*array_size, data[header_size+2:-2])
    return np.array(X),np.array(Y)
    
#save_dirc = r'J:\ORCResearch\pgrsgroup\users data store\Rex\Planar_Devices\Chirp_Loss\first-Device_19-06-12_annealed\wvgd4\rev\\'
save_dirc = r'J:\ORCResearch\pgrsgroup\users data store\Crash\Characterisation\190730 1 Micron fluence test\NB254\data\wg5\\'

if not os.path.exists(save_dirc):
    os.makedirs(save_dirc)#ensure_dir(save_dirc)
    print('making directory')
print save_dirc

print 'Starting data transfer'
Xa,Ya = readtrace('a')
Xb,Yb = readtrace('b')
#Xc,Yc = readtrace('c')
#Xd,Yd = readtrace('d')
#Xe,Ye = readtrace('e')
#Xf,Yf = readtrace('f')
#Xg,Yg = readtrace('g')

# Get PyLab and plot the data retrieved
#fig = plt.figure('ab_%')
#plt.cla()
#plt.plot(np.array(Xb)*1e9,100*10**((Ya-Yb-13.9)/10.),label='4% fibre ferrule reference Refl')

fig = plt.figure('ab')
plt.cla()
plt.plot(np.array(Xa)*1e9,Ya,label='4% (dBm)')
plt.plot(np.array(Xb)*1e9,Yb,label='Data (dBm)')
plt.plot(np.array(Xa)*1e9,Yb-Ya-14,label='Cali. Data (dB)')
#plt.plot(np.array(Xb)*1e9,Ya-Yc-13.9,label='Refl. Corrected')
#plt.plot(np.array(Xa)*1e9,Ya-Yd,label='Tran. Corrected')
plt.ylim([-85, 0])
plt.xlabel('Wavelength/(nm)')
plt.ylabel('Intensity')
#plt.ylabel('Reflectivity (%)')
plt.legend(framealpha=0.5)
plt.show()

# Free the connection
s.close()
##
#root = tk.Tk()
#root.withdraw()

plt.figure('ab')
try:
    #plt.savefig(save_dirc+'/Plot.pdf')
    np.save(save_dirc+'/Data.npy',np.vstack([Xb,Yb]))
    np.save(save_dirc+'/Background.npy',np.vstack([Xa,Ya]))
    #np.savetxt(save_dirc+'/Data.csv',np.vstack([Xa,Ya]))
    #np.savetxt(save_dirc+'/Background.csv',np.vstack([Xb,Yb]))
except:
    print('Error in saving')
#np.save(save_dirc+'/Refl_Background.npy',np.vstack([Xc,Yc]))
#np.save(save_dirc+'/Tran_Background.npy',np.vstack([Xd,Yd]))