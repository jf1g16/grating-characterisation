# -*- coding: utf-8 -*-
"""
Created on Fri Nov  1 11:27:38 2019

@author: jf1g16
"""

import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as sig
import gratCharLib as grat
from scipy.optimize import curve_fit

debugPath = 'J:/ORCResearch/pgrsgroup/users data store/Crash/Characterisation/191022_PX194_Fluence_Test/bin/Logs/Debug.txt'
try:
    if not dat.shape:
        dat = np.array(grat.importLog(debugPath))
except:

    dat = np.array(grat.importLog(debugPath))


def linear(x, *p):
    a,b = p
    return a*x + b


waveguideNo = 1#8
reverse = False
wgDat = dat[dat[:,0]==waveguideNo]
wgName = "wg"+str(waveguideNo)
if reverse is True:
    wgName = str(wgName)+'r'
    
uniArr = []
for row in wgDat:
    if (row[4].duty==0.7):
        uniArr.append(row);
uniArr = np.array(uniArr)

#name = "wg9"
pathC = "J:/ORCResearch/pgrsgroup/users data store/Crash/Characterisation/191022_PX194_Fluence_Test/Data/"+wgName+"/"
wgc = grat.gratingChar(pathC, [790, 820],1.5,wgDat, gauss=True)
#wgcr = grat.gratingChar(pathCr, [760, 810],1.5, np.flip(wgDat, axis=0), offset = 0,autoOffset = True, startNumber  = 4)
wgc.minProm=5E-4
fit = wgc.fit(plotName=wgName)

cFWHM = wgc.fitFWHM(plotName=wgName)

plt.figure(wgName+" linear")
#plt.savefig(pathC+wgName+" linear.pdf")
plt.figure(wgName+" log")
#plt.savefig(pathC+wgName+" log.pdf")
#np.savetxt(pathC+wgName+" fit.txt", cFWHM)

fittedWls = fit[:,2]
logPeriods = np.array([i.period for i in wgDat[:,3]])
nEff = grat.nEff(fittedWls, logPeriods, 1.461)[0]

print(nEff)
uniWls = np.array( [per.period*nEff*2 for per in uniArr[:,3]])
uniDist = uniArr[:,2]
uniDist = uniDist.astype('float')
pairedWls1, pairedWls2 = grat.pairWavelengths(uniWls, fit[:,2], thres=0.3)
strengthUni = []
strengthErr = []
dist = []
for idx, wl in enumerate(pairedWls2):
    idx = np.where(fit[:, 2]==wl)
    strengthUni.append([fit[idx,0][0,0], fit[idx,1][0,0]])
    
for wl in pairedWls1:
    idx = np.where(uniWls == wl)
    dist.append(uniDist[idx][0])
dist = np.array(dist)



strengthUni = np.array(strengthUni)
strengthUpper = np.abs(10*np.log(strengthUni[:,0]+strengthUni[:,1])/2-10*np.log(strengthUni[:,0])/2)
strengthLower = np.abs(10*np.log(strengthUni[:,0]-strengthUni[:,1])/2-10*np.log(strengthUni[:,0])/2)
#strengthUpper = 1*dist/np.abs(dist)
#strengthLower = 2*dist/np.abs(dist)
strengthUni = 10*np.log(strengthUni[:,0])/2
'''
strengthUni = np.delete(strengthUni, -2)
dist = np.delete(dist, -2)
strengthUpper = np.delete(strengthUpper, -2)
strengthLower = np.delete(strengthLower, -2)
'''
lossFit = np.polyfit(dist, strengthUni, 1)
lossModel = np.poly1d(lossFit)
p0 = [lossFit[0],lossFit[1]]
lossFit, lossFitVar = curve_fit(linear, dist, strengthUni, p0=p0)
lossFitErr = np.array([np.sqrt(lossFitVar[0][0]), np.sqrt(lossFitVar[1][1])])
f = plt.figure("Loss plot")
plt.clf()
plt.errorbar(dist, strengthUni, yerr = [strengthLower, strengthUpper], fmt='x')
#plt.scatter(dist, strengthUni)
#plt.scatter(dist, strengthUpper)
#plt.scatter(dist, strengthLower)

plt.plot(dist, lossModel(dist))

plt.xlabel("Distance along chip (mm)")
plt.ylabel("Loss (dB)")
print("Loss = ",-lossFit[0], ", err = ",lossFitErr[0])
strLabel = "Loss = "+str("%.2f"%abs(lossFit[0]))+" ± "+str("%.2f"%abs(lossFitErr[0])+" dB/mm")
f.text(0.2, 0.2, strLabel)
plt.savefig(pathC+wgName+" loss.pdf")
'''
plt.figure("Distance vs Wl")
plt.clf()
plt.scatter(dist, pairedWls2)
plt.xlabel("Distance along chip (mm)")
plt.ylabel("Bragg wavelength (nm)")
'''