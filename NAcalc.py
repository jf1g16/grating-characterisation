# -*- coding: utf-8 -*-
"""
Created on Thu Oct 24 08:34:00 2019

@author: jf1g16
"""

import os
import numpy as np
import imageio
import matplotlib.pyplot as plt
import cv2


'''Returns the x moment, y moment, other x moment, other y moment for an image, given an array of pixel values and the pixel pitch

x and y moment defined as usual, other moments defined as in NA standard.


'''
def moments(image, pitch):
    moment00 = 0
    moment11 = 0
    
    moment10 = 0
    moment20 = 0
    
    moment01 = 0
    moment02 = 0

    
    image = image/image.max()
    for rowNo, row in enumerate(image):
        for colNo, pixel in enumerate(row):
            moment00 = moment00 + pixel
            moment11 = moment11 + pixel*colNo*rowNo*pitch**2
            
            moment10 = moment10 + pixel*rowNo*pitch
            moment20 = moment20 + pixel*rowNo**2*pitch**2
            
            moment01 = moment01 + pixel*colNo*pitch
            moment02 = moment02 + pixel*colNo**2*pitch**2
    
    centroidX = moment10/moment00
    centroidY = moment01/moment00
    
    sigmaX = np.sqrt((moment20-centroidX*moment10)/moment00)
    sigmaY = np.sqrt((moment02-centroidY*moment01)/moment00)
    
    sigmaXX = sigmaX**2
    sigmaYY = sigmaY**2
    sigmaXY = (moment11 - centroidX*moment01)/moment00
    
    gamma = (sigmaXX - sigmaYY)/np.abs(sigmaXX - sigmaYY)
    
    a = gamma*np.sqrt((sigmaXX+sigmaYY)**2 + 4*sigmaXY**2)
    
    otherSigmaX = np.sqrt(2*(sigmaXX+sigmaYY-a))
    otherSigmaY = np.sqrt(2*(sigmaXX+sigmaYY+a))
    
    return sigmaX, sigmaY, otherSigmaX, otherSigmaY

#path = 'J:/ORCResearch/pgrsgroup/users data store/Crash/Characterisation/191022_PX194_fluence_test/data/Planar Layer 12/2019-10-23--14-16-34/'
path = 'J:/ORCResearch/pgrsgroup/users data store/Crash/Characterisation/190730 1 Micron fluence test/NB254/data/NA/wg1/2019-08-08--10-43-53/'

for imPath in os.listdir(path+'images/'):
        print(imPath)
        distance = float(imPath.split('.')[0])
        
        
        
imPath = '+78.png'        
fullPath = path+'images/'+imPath
img = imageio.imread(fullPath)
#img = testImage
plt.figure("Image")
plt.clf()

pitch = 15
mag = 57.4
pitch= pitch/mag
pitch = 1
plt.imshow(np.flip(img, axis=0), extent = [0, img.shape[1]*pitch, 0, img.shape[0]*pitch], aspect='equal')
#mom = moments(img, pitch/mag)
#2ndOrder = 


image = img

moment00 = 0
moment11 = 0

moment10 = 0
moment20 = 0

moment01 = 0
moment02 = 0


image = image/image.max()
for rowNo, row in enumerate(image):
    for colNo, pixel in enumerate(row):
        moment00 = moment00 + pixel
        moment11 = moment11 + pixel*colNo*rowNo*pitch**2
        
        moment01 = moment01 + pixel*rowNo*pitch
        moment02 = moment02 + pixel*rowNo**2*pitch**2
        
        moment10 = moment10 + pixel*colNo*pitch
        moment20 = moment20 + pixel*colNo**2*pitch**2

centroidX = moment10/moment00
centroidY = moment01/moment00

sigmaX = np.sqrt((moment20-centroidX*moment10)/moment00)
sigmaY = np.sqrt((moment02-centroidY*moment01)/moment00)

sigmaXX = sigmaX**2
sigmaYY = sigmaY**2
sigmaXY = (moment11 - centroidX*moment01)/moment00


gamma = (sigmaXX - sigmaYY)/np.abs(sigmaXX - sigmaYY)

a = gamma*np.sqrt((sigmaXX+sigmaYY)**2 + 4*sigmaXY**2)

otherSigmaX = np.sqrt(2*(sigmaXX+sigmaYY-a))
otherSigmaY = np.sqrt(2*(sigmaXX+sigmaYY+a))