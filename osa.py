# -*- coding: utf-8 -*-
"""
Created on Tue Jan 14 16:53:47 2020

@author: jf1g16
"""


''' Class to control the OSA over ethernet


'''

import numpy as np
import socket as soc
import time as t
from struct import unpack
import os

class osa:
    
    HOST = None
    PORT = 10001
    READBUFF = 1024
    
    hostname={'OSAVis': 'osavis.clients.soton.ac.uk',
              'OSA1550':'6370d91r223997.clients.soton.ac.uk'
              }
    s = None
    
    def __init__(self, osaName='OSAVis'):
        
        self.HOST = self.hostname[osaName]
        self.s = soc.socket(soc.AF_INET, soc.SOCK_STREAM)
        self.s.connect((self.HOST,self.PORT))
        
        # Send the beginning of the authentication command
        self.s.send(b'open "smithgrp"\r\n')
        
        # Receive the response and print to screen (for verification).
        data = self.s.recv(1024)
        print(data)
        
        # Send the password to the OSA and verify it is ready for commands
        self.s.send(b'QuantumOSA1\r\n')
        
        data2 = self.s.recv(1024)
        print(str(data2))

        # Send the command for binary (64-bit) data
        self.s.send(b':format:data REAL,64\r\n')

    def readData(self, trace):
            # Request the Y values of trace A
        self.s.send(b':trac:Y? tr'+trace+'\r\n')
        
        # Initialise a data buffer and retrieve the first packet of data to 'temp'.
        data = ''
        temp = self.s.recv(self.READBUFF)
        
        # While the data equals the read limit, continue as there may be more data
        while len(temp) == self.READBUFF:
            data += temp
            t.sleep(0.2) # Don't like this manual delay TODO: keep reading until data all received
            temp = self.s.recv(self.READBUFF)
        
        # Append the final temporary buffer to the data buffer
        data += temp
        
        # Show the length of the buffer
        print(len(data))
        
        # Use the binary data format header to separate the data part
        header_size = int(data[1])
        array_size = int(data[2:header_size+2])/8
        
        # Convert the byte data to IEEE 64-bit floating point numbers
        Y = unpack('d'*array_size, data[header_size+2:-2])
        
        # Request the X values of trace A
        self.s.send(b':trac:X? tr'+trace+'\r\n')
        
        # Initialise a data buffer and retrieve the first packet of data to 'temp'.
        data = ''
        temp = self.s.recv(self.READBUFF)
        
        # While the data equals the read limit, continue as there may be more data
        while len(temp) == self.READBUFF:
            data += temp
            t.sleep(0.2) # Don't like this manual delay TODO: keep reading until data all received
            temp = self.s.recv(self.READBUFF)
        
        # Append the final temporary buffer to the data buffer
        data += temp
        
        # Show the length of the buffer
        print(len(data))
        
        # Use the binary data format header to separate the data part
        header_size = int(data[1])
        array_size = int(data[2:header_size+2])/8
        
        # Convert the byte data to IEEE 64-bit floating point numbers
        X = unpack('d'*array_size, data[header_size+2:-2])
        return np.array(X),np.array(Y)
    
    
        
    
    def sweep(self, centre, span, sens = 'high1'):
        #Centre wavelength
        self.s.send(b':sens:wav:cent '+str(centre)+'nm')
        
        #Span width
        self.s.send(b':sens:wav:span '+str(span)+'nm')
        
        #Sweep sensitivity (high1 by default)
        self.s.send(b':sens:sens: '+str(sens))
        
        #Automatic number of sampling points
        self.s.send(b':sens:sweep:points:auto on')
        
        #Single sweep
        self.s.send(b':init:smode 1')
        
        #Clear status
        self.s.send(b'*CLS')
        
        #Initiate sweep
        self.s.send(b':init')
        
        #Wait for sweep to finish
        
        #Get 'operation event register'
        self.s.send(b':stat:oper:even?')
        
        #Same read loop as in readData, should work here too!.
        data = ''
        temp = self.s.recv(self.READBUFF)
        while len(temp) == self.READBUFF:
            data += temp
            t.sleep(0.2) # Don't like this manual delay TODO: keep reading until data all received
            temp = self.s.recv(self.READBUFF)
        data += temp
        
        
    '''    
    def sweep(self):
        
    def setup(self):
    ''' 
    def close(self):
        self.s.close()