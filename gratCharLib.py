# -*- coding: utf-8 -*-
"""
Created on Tue Aug  6 11:52:55 2019

@author: jf1g16
"""

import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
from functools import partial
import scipy.signal as sig
import re

'''class to hold uniform apodisation information
Decodes a string: 
UNI_APOD[####]
into a duty cycle,.
'''     
class UniformApodisation:
    duty = None
    name = 'Uniform'
    '''Takes the string and converts it into a python object holding the info.
    '''    
    def __init__(self, stringFoo):
        #self.duty = float(string.split('[')[-1].split(']')[0])
        self.duty = float(stringFoo.split('[')[-1].split(']')[0].split(',')[0])


'''class to hold uniform apodisation information
Decodes a string:
GAUSS_APOD[####]
into a duty cycle.
''' 
class GausianApodisation:
    duty = None
    name = 'Gaussian'
    '''Takes the string and converts it into a python object holding the info.
    '''    
    def __init__(self, stringFoo):
        #self.duty = float(string.split('[')[-1].split(']')[0])
        self.duty = float(stringFoo.split('[')[-1].split(']')[0].split(',')[0])
        
        
'''class to hold constant period information
Decodes a string:
CONST_PER[####]
into a period.
'''
class ConstantPeriod:
    period = None
    '''Takes the string and converts it into a python object holding the info.
    '''    
    def __init__(self, stringFoo):
        #print(string)
        self.period = float(stringFoo.split('[')[-1].split(']')[0].split(',')[0])
    
    
'''class to hold constant angle information
Decodes a string: 
CONST_ANG[####]
into an angle.
'''
class ConstantAngle:
    angle = None
    '''Takes the string and converts it into a python object holding the info.
    '''    
    def __init__(self, stringFoo):
        self.angle = float(stringFoo.split('[')[-1].split(']')[0])
        
        
'''class to hold constant angle information
Decodes a string:
CONST_PHASE
into a class. This class does nothing, and doesn't implement anything (but it's there).
'''
class ConstantPhase:
    phase = None
    '''Takes the string and converts it into a python object holding the info.
    '''  
    def __init__(self, stringFoo):
        return

        
        

#debugPath = 'J:/ORCResearch/pgrsgroup/users data store/Crash\Characterisation/191004_Gaussians_and Rotations/bin/Logs/Debug.txt'
def importLog(pathFoo):
    
    #Regular expression to pull out a string containing WG
    pattern = re.compile('.*(WG.*$)')
    
    
    '''Lists of class log names, and associated Python data classes
    '''
    
    periodDict = {"CONST_PER":ConstantPeriod}
    
    apodDict = {"UNI_APOD":UniformApodisation,
                "GAUSS_APOD":GausianApodisation}
    
    phaseDict = {"CONST_PHASE":ConstantPhase}
    
    angleDict = {"CONST_ANG":ConstantAngle}
    
    data = []
    
    for i, line in enumerate(open(pathFoo)):
        for match in re.finditer(pattern, line):
            string = match.group()
            stringList = string.split(" ")
            wgNo = int(stringList[3].split('[')[-1].split(']')[0])
            gratNo = int(stringList[4].split('[')[-1].split(']')[0])
            path = float(stringList[5].split('[')[-1].split(']')[0])
            period = periodDict[stringList[6].split('[')[0].split(']')[0].split(',')[0]](stringList[6])
            apod = apodDict[stringList[7].split('[')[0].split(']')[0].split(',')[0]](stringList[7])
            angle = angleDict[stringList[9].split('[')[0].split(']')[0].split(',')[0]](stringList[9])
            phase = phaseDict[stringList[8].split('[')[0].split(']')[0].split(',')[0]](stringList[8])
            length = float(stringList[10].split('[')[-1].split(']')[0])
            data.append([wgNo, gratNo, path, period, apod, angle, phase, length])
    
    return data
            

def gaussModel(x, *p): #Gaussian model for fitting
    A, mu, width= p
    return (A*np.exp(-(x-mu)**2/(width**2)))

def sincModel(x, *p): #Gaussian model for fitting
    A, mu, width= p
    return A*(np.sinc((x-mu)/(2*width)))**2

def multiFitFunc(fitfunc, N, x, *args):
    """Given a fitting function and a list of arguments, add len copies
    
    Takes:
    fitfunc --- a function of M + 1 variables
    x ---the abcissa for all the functions
    N --- the number of instances of fitfunc to be added
    *args --- a list of NM variables to ace as the initial guesses.
        The easiest way to get this in the right format is stacking
        then flattening the parameters in numpy:
            
        EXAMPLE:
            np.stack((data_percent[peaks],wlbg[peaks], widths*wl_spacing)).flatten()
            
    Returns:
        sum of fitfunc(x, args) for each entry in the list.
        """
    M = len(args)//N
    args = np.asanyarray(args).reshape(M,N).T #magic array reassembly
    out = np.zeros_like(x)
    for fargs in args:
        out += fitfunc(x, *fargs)
    return out
    
def multiFitFunc2(fitFuncList, x, *args):
    """Given a fitting function and a list of arguments, add len copies
    
    Takes:
    fitFuncList --- a list of functions to be fitted, all with M + 1 variables
    x ---the abcissa for all the functions
    *args --- a list of NM variables to ace as the initial guesses.
        The easiest way to get this in the right format is stacking
        then flattening the parameters in numpy:
            
        EXAMPLE:
            np.stack((data_percent[peaks],wlbg[peaks], widths*wl_spacing)).flatten()
            
    Returns:
        sum of fitfuncList[idx](x, args) for each entry in the list.
        """
    N = len(fitFuncList)
    M = len(args)//N
    args = np.asanyarray(args).reshape(M,N).T #magic array reassembly
    out = np.zeros_like(x)
    for idx,fargs in enumerate(args):
        out += fitFuncList[idx](x, *fargs)
    return out
    



#path = "J:/ORCresearch/pgrsgroup/users data store/crash/characterisation/190730 1 micron fluence test/ NB254/data/wg1"
path = "J:/ORCResearch/pgrsgroup/users data store/Crash/Characterisation/190730 1 Micron fluence test/NB254/data/Gratings/wg2/"
backName = "Background.npy"
datName = "Data.npy"

'''Class to load and fit gratings using the multiFitFunc.
Data must be .npy format
'''
class gratingChar:
    wlMin = None
    wlMax = None
    lpFreq = 1.5
    lpSamp = 2
    minProm = 0.0005
    minPeakWlSpacing = 0.6
    wlRes = None
    noiseFloor = -80
    datName = "Data.npy"
    backName = "Background.npy"
    path = None
    polariser =None
    figPeaks = None
    axPeaks = None
    fig = None
    ax = None
    filt = None
    gauss = None
    offset = None
    autoOffset = None
    wgDat = None
    startNumber = None
    funcInfo = None
    '''Instantiation method.
    @param dataPath - Path to the folder containing data and background (ends in /).
    @param wlRange - List of [minimmum wavelength, maximum wavelength] in nm.
    @param gratingSpacing - Minimum spacing between gratings in nm.
    @param wgDat - C++ log file array for single waveguide. @see importLog
    @kwarg filt - Bool to set whether data is lowpass filtered before peak finding
    @kwarg polariser - Bool are gratings single polarisation mode.
    @kwarg offset - Extra linear offset (to account for discrepancy between background and reflected) in percent.
    @kwarg autoOffset - If true sets offset to suggested value (overrides user input). If false uses user value
    @kwarg startNumber - Index of wgDat for first grating. Defaults at 0.
    '''
    def __init__(self, dataPath, wlRange, gratingSpacing,wgDat, polariser=True, filt = True, gauss=True, offset=0, autoOffset = False, startNumber = 0):
        self.path = dataPath
        self.wlMin = wlRange[0]
        self.wlMax = wlRange[1]
        self.minPeakWlSpacing = gratingSpacing
        self.polariser = polariser
        self.filt = filt
        self.gauss = gauss
        self.offset = offset/100
        self.autoOffset = autoOffset
        self.wgDat = wgDat
        self.startNumber = startNumber
        
    ''' Method to load data and fit.
    @returns Array of grating fit parameters. Different format depending on polariser value.
    '''
    def fit(self, plots=True, plotName = ""):
        
   #Loads data in .npy format
       dat = np.load(self.path+self.datName)
       back = np.load(self.path+self.backName)
       wls = dat[0]*1E9
       dat = dat[1]
       back = back[1]
       wlRes =wls[1]-wls[0]
       dn = self.minPeakWlSpacing/wlRes
       #print(dat.size, back.size, wls.size)
   #Calculates offset in log domain. Otherwise screws with multiFit
   #Often there's a constant broadband offset due to crap on the end of the fibre
   #when the background is taken. This suggests a value for this offset (though leaves it to the user to set it)
       
       if (self.wlMin>min(wls)):
           startDat = dat[wls<self.wlMin]
           startBack =back[wls<self.wlMin]
           start = startDat-startBack-13.98
           start = 10**(start/10)
           offset = np.average(start)
       else:
           startDat = dat[wls<(wls.min()+10)]
           startBack =back[wls<(wls.min()+10)]
           start = startDat-startBack-13.98
           start = 10**(start/10)
           offset = np.average(start)
       if not self.autoOffset:
           print("Suggested offset = ",100*offset, "%")
       else:
           print("Setting offset to ",100*offset, "%")
           self.offset= offset

      
   #Noise floor trimming
   # If data is too far below the noise floor it screws with background 
   # subtraction. This fixes it.
       dat[dat<self.noiseFloor]   = self.noiseFloor
       back[back<self.noiseFloor] = self.noiseFloor

   #Background subtraction
       logRefl = dat-back-13.98
       linRefl = 10**((logRefl)/10)-self.offset

    
   #Lowpass filtering
       filtered = None
       filteredwl = None
       if (self.filt):
           b,a = sig.butter(self.lpSamp, self.lpFreq, 'lp', fs =1/(wlRes),output='ba')
           filtered = sig.filtfilt(b, a, linRefl)
           filtered = filtered[(wls<self.wlMax)&(wls>self.wlMin)]
           filteredwl = wls[(wls<self.wlMax)&(wls>self.wlMin)]
       else:
           filtered = linRefl[(wls<self.wlMax)&(wls>self.wlMin)]
           filteredwl = wls[(wls<self.wlMax)&(wls>self.wlMin)]
  


   #Calculate peak characteristics
       peaks = np.array(sig.find_peaks(filtered, distance=dn,prominence=(self.minProm, np.inf)))[0]
       if (peaks.size ==0):
           plt.figure("No peaks found")
           plt.clf()
           plt.plot(wls, filtered)
           raise ValueError("No peaks found")
           
       else:
           print("Found ", peaks.size, " peaks")
       centres = filteredwl[peaks]
       widths,_,_,_ = sig.peak_widths(filtered, peaks, rel_height=1/np.e)
       widths = widths*wlRes
       heights = filtered[peaks]
       
   #Peak plotting
       if (plots):
           if (self.figPeaks == None):
               if (plotName == ""):
                   self.figPeaks = plt.figure()
               else:
                   self.figPeaks = plt.figure(plotName+" linear")
           self.figPeaks.clf()
           self.axPeaks = self.figPeaks.add_subplot(111)
           #self.axPeaks.set_title(self.path)
           self.axPeaks.plot(filteredwl, 100*filtered, 'C1--', zorder=5)
           self.axPeaks.plot(wls, 100*linRefl)
           
           self.axPeaks.scatter(centres, 100*heights, zorder=10, c='C1')
           self.axPeaks.set_xlabel("Wavelength (nm)")
           self.axPeaks.set_ylabel("Reflectivity (%)")
           
       
       
   #Setting limits for fits
       croppedInten = linRefl[(wls<self.wlMax)&(wls>self.wlMin)]
       croppedWls = wls[(wls<self.wlMax)&(wls>self.wlMin)]
       fitfunc = None
       lowerBoundsHeights = []
       upperBoundsHeights = []
       lowerBoundsInf = []
       upperBoundsInf = []
       upperBoundsWidth = []
       lowerBoundsWidth = []
       for idx  in range(0, heights.size):
           lowerBoundsHeights.append(0)
           lowerBoundsInf.append(-np.inf)
           lowerBoundsWidth.append(0)
           upperBoundsHeights.append(np.inf)
           upperBoundsInf.append(np.inf)
           upperBoundsWidth.append(widths[idx]*2)

    #Function setup for fits.
       funcInfo = []
       #wgDat is sorted in order of distance along waveguide. Functions need to be provided in order of period.
       
       for row in self.wgDat:
           funcInfo.append([row[3].period, row[4].name])
       funcInfo = np.array(funcInfo)
       funcs = []
       funcInfo = funcInfo[np.argsort(funcInfo, 0)[:,0]]
       self.funcInfo = funcInfo
       
       
       for i in range(0, len(centres)):
           #Pulls function names from log file to plot different grating envelopes automatically.
           #print(i, funcInfo[i+self.startNumber])
           if (i+self.startNumber >= len(self.wgDat)):
               raise ValueError('startNumber given to grat class is too high! (or too many peaks found)')
           if (funcInfo[i+self.startNumber,1] == 'Uniform'):
               funcs.append(sincModel)
           elif (funcInfo[i+self.startNumber,1] == 'Gaussian'):
               funcs.append(gaussModel)
       fitfunc = partial(multiFitFunc2, funcs)
       initial_guess=np.stack((heights, centres, widths)).flatten()
       lowerBounds = np.stack((lowerBoundsHeights, lowerBoundsInf, lowerBoundsWidth)).flatten()
       upperBounds = np.stack((upperBoundsHeights, upperBoundsInf, upperBoundsWidth)).flatten()
       fits, var_matrix = curve_fit(f = fitfunc, xdata = croppedWls, ydata = croppedInten, p0 = initial_guess, bounds = [lowerBounds, upperBounds])
       devs = []
       for i in range(len(fits)): #Makes a std_dev array for all fir parameters
           try:
               devs.append(np.absolute(var_matrix[i][i])**0.5) #If fails, puts zero
           except:
               devs.append(float('nan'))
       devs = np.array(devs)
       if(plots):
           if (self.fig == None):
               if (plotName == ""):
                   self.fig = plt.figure()
               else:
                   self.fig = plt.figure(plotName+" log")
           self.fig.clf()
           self.ax = self.fig.add_subplot(111)
           #self.ax.set_title(self.path)
           self.ax.plot(wls, logRefl)
           #return fitfunc(wls, *fits)
           #10*np.log10(fitfunc(wls, *fits))
           preLog = fitfunc(wls, *fits)+self.offset
           
           #Truncates log data at -160dB to prevent warnings in np.log
           
           preLog[preLog<1E-16] = 1E-16
           self.ax.plot(wls, 10*np.log10(preLog))
           self.ax.set_xlabel("Wavelength (nm)")
           self.ax.set_ylabel("Reflectivity (dB)")
           self.axPeaks.plot(wls, 100*fitfunc(wls, *fits))
           self.ax.set_ylim([logRefl.min()*1.1-0.1*logRefl.max(), logRefl.max()*1.1-logRefl.min()*0.1])
       
   #Array shaping and returning
   
       #If no polariser, returns a 2xn//2x6 array of fit parameters
       #return[0][0] ~ height0, stddevHeight0, centre0, stddevcentre0, width0, stddevwidth0
       #return[0][1] ~ height1, stddevHeight1, centre1, stddevcentre1, width1, stddevwidth1
       #etc.
       #return[0] is for te, return[1] is for tm
       #return fits, devs
       if not self.polariser:
           
           gratings = fits.T.reshape(3, centres.size).T
           gratingsErrs = devs.T.reshape(3, centres.size).T
           #return gratings, gratingsErrs, centres.size
           if (centres.size%2==1):
               gratings = np.delete(gratings, -1, axis=0)
               gratingsErrs = np.delete(gratingsErrs, -1, axis=0)
           print(gratings.shape, gratingsErrs.shape)
           teGratings = np.zeros([centres.size//2, 6])
           tmGratings = np.zeros([centres.size//2, 6])
           print(teGratings.shape, tmGratings.shape)
           teGratings[:,0::2] = gratings[0::2]
           teGratings[:,1::2] = gratingsErrs[0::2]
            
           tmGratings[:,0::2] = gratings[1::2]
           tmGratings[:,1::2] = gratingsErrs[1::2]
           
           #print(tmGratings.shape, teGratings.shape)
       else:
           
       #If polariser, returns a nx6 array of fit parameters
       #return[0] ~ height0, stddevHeight0, centre0, stddevcentre0, width0, stddevwidth0
       #return[1] ~ height1, stddevHeight1, centre1, stddevcentre1, width1, stddevwidth1
       #etc.

           gratings = fits.T.reshape(3, centres.size).T
           gratingsErrs = devs.T.reshape(3, centres.size).T
           #return ([gratings, gratingsErrs, centres.size])
           retGratings = np.zeros([centres.size, 6])
           retGratings[:, 0::2] = gratings
           retGratings[:, 1::2] = gratingsErrs

           return retGratings
       
    def fitFWHM(self, plots=False, plotName = ""):
            fits = self.fit(plots=plots, plotName = plotName)
            
            for idx, fit in enumerate(fits):
                height, heightErr, centreWl, centreWlErr, width, widthErr = fit
                wlRange = np.arange(centreWl-np.abs(width)*4, centreWl+np.abs(width)*4, 0.01)
                funcArr = None
                p = [height, centreWl, width]
                if (self.gauss):
                    funcArr = gaussModel(wlRange, *p)
                else:
                    funcArr = sincModel(wlRange, *p)
                funcArr = funcArr/height
                funcArr = np.abs(funcArr-0.5)
                fwhmIdx = np.where(funcArr == funcArr.min())[0]
                fwhm = np.abs(centreWl - wlRange[fwhmIdx])*2
                fwhmError = widthErr*fwhm/width 
                #  print(fits, fwhmIdx)
                fits[idx,4] = fwhm
                fits[idx,5] = fwhmError
                
            return fits
 
'''Function to programmatically match 2 sets of wavelengths (for calculating nEff or doing loss measurements)
'''        
def pairWavelengths(wavelengths1, wavelengths2, thres = 0.2):
    
    #thres = 0.2
    
    wls1Paired = []
    wls2Paired = []
    
    for wl in wavelengths1:
        idx = (np.abs(wl-wavelengths2)).argmin()
        if np.abs(wl-wavelengths2[idx]) < thres:
            wls2Paired.append(wavelengths2[idx])
            wls1Paired.append(wl)
    
    return np.array(wls1Paired), np.array(wls2Paired)

def nEff(fittedWls,periods, nGuess):
    
    pairedWls, pairedPeriods = pairWavelengths(fittedWls, periods*2*nGuess, 5)
    pairedPeriods = np.array(pairedPeriods)/(2*nGuess)
    
    sbb = 0
    sbl = 0
    sll = 0
    for i in range(0, pairedWls.size):
        #print(pairedWls[i], pairedPeriods[i])
        
        bb = pairedWls[i]**2
        sbb = sbb + bb
        
        ll = pairedPeriods[i]**2
        sll = sll+ll
        
        bl = pairedPeriods[i]*pairedWls[i]
        sbl = sbl + bl
        
       # print(bb, bl, ll)
    #print(sbb, sbl, sll)
    n = sbl/(2*sll)
    nRes = 0
    for i in range(0, pairedWls.size):
        dNRes = (n - pairedWls[i]/(pairedPeriods[i]*2))**2
        nRes = nRes + dNRes
        #print(dNRes)
    dn = np.sqrt(nRes/pairedWls.size)
    return n, dn