# -*- coding: utf-8 -*-
"""
Created on Wed Nov 13 11:37:32 2019

@author: jf1g16
"""

import numpy as np
import matplotlib.pyplot as plt

path = 'J:/ORCResearch/pgrsgroup/users data store/Crash/Characterisation/191113_PX194_Focal_Length/Pre write firing pos tests/'
fname = 'Wg1pre_transform.txt'

n = 1.46

dat = np.loadtxt(path+fname)
dat = dat[:,0]


diffE = -dat[0::2][0:dat[2::2].size]+dat[2::2]
diffO = -dat[1::2][0:dat[3::2].size]+dat[3::2]

perE = diffE*1E6
perO = diffO*1E6
distE = dat[0::2][0:dat[2::2].size]
distO = dat[1::2][0:dat[3::2].size]

plt.figure('Periods')
plt.clf()
plt.plot(distE, perE, label ='Even')
plt.plot(distO, perO, label='Odd')
plt.legend()