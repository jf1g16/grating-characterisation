# -*- coding: utf-8 -*-
"""
Created on Tue Sep 10 10:23:50 2019

@author: jf1g16
"""

import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as sig
import gratCharLib as grat

debugPath = 'J:/ORCResearch/pgrsgroup/users data store/Crash/Characterisation/191022_PX194_Fluence_Test/bin/Logs/Debug.txt'
try:
    if not dat.shape:
        dat = np.array(grat.importLog(debugPath))
except:

    dat = np.array(grat.importLog(debugPath))



waveguideNo = 1
reverse = False
wgDat = dat[dat[:,0]==waveguideNo]
wgName = "wg"+str(waveguideNo)
if reverse is True:
    wgName = str(wgName)+'r'

#name = "wg9"
pathC = "J:/ORCResearch/pgrsgroup/users data store/Crash/Characterisation/191022_PX194_Fluence_Test/Data/"+wgName+"/"
wgc = grat.gratingChar(pathC, [772, 784],1.5, gauss=False)
wgc.minProm=5E-4
fit = wgc.fit(plotName=wgName)

cFWHM = wgc.fitFWHM(plotName=wgName)

plt.figure(wgName+" linear")
plt.savefig(pathC+wgName+" linear.pdf")
plt.figure(wgName+" log")
plt.savefig(pathC+wgName+" log.pdf")
np.savetxt(pathC+wgName+" fit.txt", cFWHM)

fittedWls = fit[:,2]
logPeriods = np.array([i.period for i in wgDat[:,3]])

print(grat.nEff(fittedWls, logPeriods, 1.445))