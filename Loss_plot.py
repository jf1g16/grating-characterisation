# -*- coding: utf-8 -*-
"""
Created on Tue Sep 10 10:23:50 2019

@author: jf1g16
"""

import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as sig
import gratCharLib as grat
from scipy.optimize import curve_fit

def linearModel(x, *a):
    offset, gradient = a
    return offset + x*gradient

debugPath = 'J:/ORCResearch/pgrsgroup/users data store/Crash/Characterisation/191104_PX194_Fluence_Test/bin/Logs/Debug.txt'
try:
    if not dat.shape:
        dat = np.array(grat.importLog(debugPath))
except:

    dat = np.array(grat.importLog(debugPath))


#plt.close('all')
waveguideNo = 14
wgDat = dat[dat[:,0]==waveguideNo]
wgName = "wg"+str(waveguideNo)

prom = 1E-3
nEff = 1.46
gratSpacing = 4
weight = True


pathC = 'J:/ORCResearch/pgrsgroup/users data store/Crash/Characterisation/191104_PX194_Fluence_Test/Data/'+wgName+"/"
wgc = grat.gratingChar(pathC, [760, 797],gratSpacing, wgDat, autoOffset = True, startNumber  = 4)
wgc.minProm= prom
fit = wgc.fit(plotName=wgName)

pathCr = 'J:/ORCResearch/pgrsgroup/users data store/Crash/Characterisation/191104_PX194_Fluence_Test/Data/'+wgName+"r/"
wgcr = grat.gratingChar(pathCr, [760, 797], gratSpacing, np.flip(wgDat, axis=0),autoOffset = True, startNumber  = 4)
wgcr.minProm=prom
fitr = wgcr.fit(plotName=wgName+'r')

cFWHM = wgcr.fitFWHM(plotName=wgName+'r')

plt.figure(wgName+" linear")
plt.savefig(pathC+wgName+" linear.pdf")
plt.figure(wgName+" log")
plt.savefig(pathC+wgName+" log.pdf")
np.savetxt(pathC+wgName+" fit.txt", cFWHM)


#Pulls the centre wavelengths out of the fits.
fittedWls = fit[:,2]
fittedWlsr = fitr[:,2]

#Creates a new array from the log file, with only the periods and the distance along the waveguide for each grating
datPeriods = np.array([[row[3].period, row[2]] for row in wgDat])

#Calculates nEff from an estimate
nEff = grat.nEff(fittedWls, datPeriods[:,0], nEff)
nEffErr, nEff = nEff[1], nEff[0]


#Pairs forward and backward wavelengths. Paired list is ordered based on first list, and only contains wavelengths
# that are below a threshold difference from each other (these are considered 'matched')
pairedWls, pairedWlsr = grat.pairWavelengths(fittedWls, fittedWlsr)

#Calculates an index (for the fit arrqy) for paired wavelengths.
pairIdx = [np.where(wl==fittedWls)[0][0] for wl in pairedWls]
pairIdxr = [np.where(wl==fittedWlsr)[0][0] for wl in pairedWlsr]


#Pulls out the strength for each wavelength in the paired list
pairStrength = fit[pairIdx,0]
pairStrengthr = fitr[pairIdxr,0]
pairStrengthErr = fit[pairIdx,1]
pairStrengthrErr = fitr[pairIdxr,1]
#Calculates the ratio of forward and reverse strength in dB
logRatio = 10*np.log(pairStrength)/np.log(10) -10*np.log(pairStrengthr)/np.log(10)
logRatioErr= 10/np.log(10)*(pairStrengthErr/pairStrength + pairStrengthrErr/pairStrengthr)



#Calculates the bragg wavelengths from the config file (In order of distance along wavegide)
datWls = datPeriods[:,0]*nEff*2

#Pulls the distances of all the gratings into a separate array
datDistances = datPeriods[:,1]


#Pairs fitted wavelengths and config wavelengths. Assumes that all fitted wavelengths should exist in the 
# config, so pairs each fitted wavelength with the closest to it in config (no matter what the difference is).
foo, datPaired = grat.pairWavelengths(pairedWls, datWls, thres = np.inf)

#Calculates the index of all matched config wavelengths, relative to wavelength order.
# used to reorder distances in terms of grating wavelengths
datIdx = [np.where(wl==datWls)[0][0] for wl in datPaired]

#Reorders distances into wavelength order.
orderDistances = datDistances[datIdx]

#Calculates loss from gradient (Helen's method). Uses np.polyfit for an estimate
# Uses estimate from polyfit as estimate for parameters for curve fit.
fitEst = np.polyfit(orderDistances*4, logRatio, 1)
p0 = [fitEst[1], fitEst[0]]
lossFit = None
lossVar = None
if weight is True:
    lossFit, lossVar = curve_fit(linearModel, orderDistances, logRatio, p0=p0, sigma = logRatioErr, absolute_sigma=True)
else:
    lossFit, lossVar = curve_fit(linearModel, orderDistances, logRatio, p0=p0)

#Uses curve_fit variance matrix to calculate standard errors of fit parameters.
lossErr = [np.sqrt(var[idx]) for idx,var in enumerate(lossVar)]


f=plt.figure("Waveguide Loss2")
plt.clf()
plt.errorbar(orderDistances, logRatio, yerr = logRatioErr, fmt='x')
plt.xlabel('Distance (mm)')
plt.ylabel('Ratio of grating reflectivities (dB)')
plt.plot(orderDistances, linearModel(orderDistances, *lossFit))
print('Loss = ',-lossFit[1]*10/4," ± ", lossErr[1]*10/4)
strLabel = "Loss = "+str("%.2f"%abs(lossFit[1]*10/4))+" ± "+str("%.2f"%abs(lossErr[1]*10/4)+" dB/cm")
f.text(0.2, 0.2, strLabel)
plt.savefig(pathC+wgName+" loss.pdf")



